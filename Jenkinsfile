import groovy.transform.Field

def distCreated = false

def verifyStageUnstable = false
def deployToProd = true
def doShowEnvironment = true 
def doConditional = true 
def doGetAwsServiceAccountCreds = true 
def doInstallVariousTools = true
def doDevStateful = true 
def doDevStateless = true 
def doFireArtilleryTestsDev = true
def doDeployToPreProd = true 
def doPreProdStateful = true 
def doPreProdStateless = true 
def doFireArtilleryTestsPreProd = true 
def doProdStateful = true 
def doProdStateless = true 
def doPreProdTearDown = true
def doBuildTheApp = true 
def doFunctionalTests = true 
def doDeployTheAppDev = true 
def doDeployTheAppProd = true

@Field def inputTtl     = 5

def setAppVersionInEnv() {

// Pulls the version number from the package.json and stores in a env var 
// env.CURRENT_VERSION

   script {
      env.CURRENT_VERSION = sh(
         script: "node -e \"console.log(require('./package.json').version);\"",
         returnStdout: true
      ).trim()
   }
}

def deployTheAppDev() {

   setAppVersionInEnv()

// Upload the bundle to S3 - dev

   withCredentials([[$class: 'AmazonWebServicesCredentialsBinding', credentialsId: 'webappsdev-serviceacc']]) 
   {

// The build should create this, so remove it later

      createTempDist()

// Upload bundle to S3 bucket

      sh "aws s3 cp --content-type 'text/*' dist/subscription_service_bundle.zip s3://${params.AWS_DEV_S3_NAME}/subscription_service/${env.CURRENT_VERSION}/"

   } // End with creds
}

def createTempDist() {
   sh "pwd"
   sh "mkdir dist"
   sh "echo '' > ./dist/subscription_service_bundle.zip"
}


def deployTheAppProd() {
   setAppVersionInEnv()

// Upload to Pre-prod S3 bucket for use by Lambda

   withCredentials([[$class: 'AmazonWebServicesCredentialsBinding', credentialsId: 'webappsprod-serviceacc']]) 
   {

      sh "aws s3 cp --content-type 'text/*' dist/subscription_service_bundle.zip s3://${params.AWS_PROD_S3_NAME}/pre-prod-artifacts/subscription_service/1.0.0/"
   }
}


def deployTheApiProd() {

   return;

// Upload swagger to API Portal S3 bucket, And run job to create client

   withCredentials([[$class: 'AmazonWebServicesCredentialsBinding', credentialsId: 'webappsprod-serviceacc']]) 
      {
         sh "aws s3 cp dist/swagger.json s3://${params.AWS_S3_REST_API_PORTAL_BUCKET_NAME}/subscription_service_swagger.json"
         build(
            job: 'ProductDevelopment/Webapps/webapps-client-updates',
            parameters: [[$class: 'StringParameterValue', name: 'AWS_S3_KEY', value: "subscription_service_swagger.json"]],
               propagate: false,
               wait: false
         )
      }
}



def placeHolder() {
   sh '''
      showVars="N"

      if [ $showVars == "Y" ] 
         then
            echo "ID=" + ${AWS_ACCESS_KEY_ID}
            echo "SEC=" + ${AWS_SECRET_ACCESS_KEY}

            echo "TF_USER=" + ${TF_VAR_okta_username}
            echo "TF_PWD=" + ${TF_VAR_okta_password}
            echo "TF_ID=" + ${TF_VAR_okta_client_id} 
            echo "TF_SC=" + ${TF_VAR_okta_client_secret} 

            echo "Priv=" + ${private_key} 

            echo "home is=" ~
         fi
   '''
}


def doTerraformDestroy(tFvars, backendTFvars, type, buildenv) {

}

def doAPwd() {
   sh '''
      pwd
   '''
}

def doRsaPiece() {

// Create a id_rsa for ssh access to bitbucket and updates known_hosts

    echo "doRsaPiece ------------"

    sh '''
           mkdir -p /home/jenkins/.ssh
           cp ${private_key} /home/jenkins/.ssh/id_rsa
           echo ${private_key}
           chmod 600 /home/jenkins/.ssh/id_rsa
           cat /home/jenkins/.ssh/id_rsa
           touch /home/jenkins/.ssh/known_hosts
           ssh-keyscan -t rsa bitbucket.org >> /home/jenkins/.ssh/known_hosts
           cat /home/jenkins/.ssh/known_hosts
    '''

    echo "doRsaPiece ------------"
}

def number=1

def doTerraformStep(tFvars, backendTFvars, type, buildenv) {

//echo "---- number= $number"

  //echo "---- doTerraformStep "
  //echo "tfVars= $tFvars"
  //echo "backendTFvars= $backendTFvars"
  //echo "type= $type"
  //echo "buildenv= $buildenv"

// Groovy closure to send slack message. Much like anonymous function.

  def slackClosure = {
    slackSend(channel: params.SLACK_NOTIFICATION_CHANNEL_NAME, attachments: [
       [
           text    : it, // "it" is a reserve word in groovy. 
                         // Used for the argument passed to the closure.
           fallback: "Job waiting approval: ${currentBuild.fullDisplayName}",
           color   : 'good',
           actions : [
               [
                   "type": "button",
                   "text": "Approve Terraform Plan",
                   "url" : env.BUILD_URL
               ]
           ]
       ]
    ])           // End slack send
  }              // End def slackClosure


// If buildenv is equal to dev, use dev creds, else use prod creds

  awsCreds = (buildenv == "dev") ? "webappsdev-serviceacc" : "webappsprod-serviceacc"

  withCredentials([
      sshUserPrivateKey(credentialsId: "terraform-common-read-only", keyFileVariable: 'private_key'),
      [$class: 'AmazonWebServicesCredentialsBinding', credentialsId: awsCreds],
      [$class: 'UsernamePasswordMultiBinding', credentialsId: 'arc_okta_service_user', 
         usernameVariable: 'TF_VAR_okta_username', passwordVariable: 'TF_VAR_okta_password'],
         string(credentialsId: 'arc_okta_client_id', variable: 'TF_VAR_okta_client_id'),
      string(credentialsId: 'arc_okta_client_secret', variable: 'TF_VAR_okta_client_secret'),
  ]) {

// setup ssh key for cloning module from private repo

    doRsaPiece()

    sh "pwd"
    sh "echo $backendTFvars"

    sh "/usr/local/bin/terraform init -input=false -backend-config=$backendTFvars"

// was "webapps-dev-backend.tfvars"


    echo "Init was good............."

    script {
      terraformPlanReturnCode = sh(
          script: "/usr/local/bin/terraform plan -detailed-exitcode -input=false -out=tfplan -var-file=$tFvars -var-file=$backendTFvars",
          returnStatus: true
      )

      echo "Plan was good............."

      if (1 == terraformPlanReturnCode) {
        // 1 = Error
        error "Terraform Plan failed with Exception."
      } else if (2 == terraformPlanReturnCode) {
        // 2 = Succeeded with non-empty diff (changes present)
        // Notify Slack there are pending Terraform approvals
        terraformPlan = sh(
            script: '/usr/local/bin/terraform show tfplan',
            returnStdout: true
        )

// If stateful infrastructure has diff, pause for more thorough manual review. 
// User must type "yes" to proceed

// If stateless infrastructure has diff, autodeploy in nonproduction environments, 
// for production standard proceed/abort input remains

// Both inputs timeout after 5 min

        if (type == "stateful") {
          if (buildenv == "preprod") {
            sh "/usr/local/bin/terraform apply -input=false tfplan"
          } else {
            slackClosure.call(terraformPlan)
            timeout(time: inputTtl, unit: 'MINUTES') {
              userInput = input(
                  id: 'userInput', message: 'Apply changes to DynamoDB? Please type "yes" to continue...', parameters: [
                  [$class: 'TextParameterDefinition', description: 'Only typing "yes" will allow job to proceed', name: 'applyStateful']
              ]
              )
            }

            if (userInput == "yes") {
              sh "/usr/local/bin/terraform apply -input=false tfplan"
            } else {
              currentBuild.result = 'ABORTED'
              error('Stateful changes not applied. Aborting...')
            }

          }
        } else {
          if (buildenv == "prod") {
            slackClosure.call(terraformPlan)
            timeout(time: inputTtl, unit: 'MINUTES') {
              input 'Apply Plan'
            }
          }
          sh "/usr/local/bin/terraform apply -input=false tfplan"
        }
      }
    }
  }
}




def fireArtillery(buildenv, branchName) {

   echo "fireArtillery for env="+buildenv+" for branch="+branchName
    
   echo "Doing nothing right now"

}

def createTokenFileForNpmRepo() {

  withCredentials([string(credentialsId: 'NPM_TOKEN', variable: 'NPM_TOKEN')]) {
    writeFile(file: ".npmrc", text: "//nexus.arc.travel/repository/npm-private/:_authToken=${env.NPM_TOKEN}")
  }
}



// Test to make sure we can connect and list the s3 buckets

def getAwsServiceAccountCreds() {

//  This tests out withCredentials for our webappsdev-serviceacc

   echo "--- getAwsServiceAccountCreds"

   sh '''
      aws sts get-caller-identity
   '''
   
   awsCreds="webappsdev-serviceacc"

   withCredentials([
      
      [$class: 'AmazonWebServicesCredentialsBinding', credentialsId: awsCreds]]) {

         echo "Got AWS creds " 

         sh '''
            echo "List the s3 buckets"
            aws s3 ls 
            echo "-------------------"
         '''
      }
}
   



def showEnvironment() {
   echo "--- Environment"

   sh '''
      cat /tmp/slave

      uname -a

#     ls /

      whoami
      cat /etc/os-release
      
      /usr/local/bin/terraform -version
#     which terraform

      node --version
      npm -version
      python3 -V
      pip3 -V

      ls /tmp/
      ls /tmp/node_modules | grep "got"


   '''

   echo "--- Environment"
   
}

def installPython3() {
   echo "--- installPython3"
   sh ''' 
      sudo apt update
      sudo apt install python3-pip -y
      pip3 --version
   '''
}

def installNodejs() {
   echo "--- installNodeJs"
   sh '''
      curl -sL https://deb.nodesource.com/setup_10.x | bash -
      apt-get install -y nodejs
      apt-get install -y zip unzip
      apt-get install -y uuid-runtime
      apt-get install -y jq
      node -v
      npm -v
   '''
}

def buildTheApp() {
   echo "--- buildTheApp"
// echo ${ARC_CLIENT_ID}
   sh '''
      yarn install
      echo "ARC_CLIENT_ID=" ${ARC_CLIENT_ID}
#     yarn run auth $ARC_CLIENT_ID $ARC_CLIENT_SECRET $USERNAME $PASSWORD
   '''

}


// Start here

pipeline {

  agent { label 'dind-webapps-green' }  // The slave to use

  options {
// Checkout will not occur is this is set to true, others may fail down the line as a
// result
//  skipDefaultCheckout true
    skipDefaultCheckout false
  }

// Set these params, these show up as the build params in the web interface

  parameters {

    string(name: 'AWS_DEV_S3_NAME', 
           defaultValue: 'tip-dev-artifacts', 
           description: 'Name of tip dev S3 Bucket')

    string(name: 'AWS_PROD_S3_NAME', 
           defaultValue: 'tip-prod-artifacts', 
           description: 'Name of tip prod S3 Bucket')

    string(name: 'AWS_S3_REST_API_PORTAL_BUCKET_NAME',  
           defaultValue: 'arc-webapps-dev-portal-artifacts/catalog',
           description: 'Name of S3 Bucket for Swagger JSON Used for REST API Portal')

    string(name: 'TF_WORKSPACE', defaultValue: 'default')

    string(name: 'SLACK_NOTIFICATION_CHANNEL_NAME', defaultValue: '#webapps-jenkins')
  }


  environment {
    TF_WORKSPACE = "${params.TF_WORKSPACE}"
  }



  stages {

// Just a conditional test

    stage('Conditional') {
       when { equals expected: true, actual: doConditional }
          steps {
            echo "conditional........."
          }
    }

// Do this first will show all installed build tools from slave

    stage('ShowEnvironment') {

       when { equals expected: true, actual: doShowEnvironment }

          steps {
// Will show current state of the slave with all versions creation time etc
             showEnvironment()
          }
    }

    stage('getAwsServiceAccountCreds') {

       when { equals expected: true, actual: doGetAwsServiceAccountCreds }

// Just to test out creds

       steps {
          getAwsServiceAccountCreds()
       }

    }

    stage('Install various tools') {

      when { equals expected: true, actual: doInstallVariousTools }
        steps {
           echo "Install various Tools"
        }

      post {
        failure {
           echo "--- Failed in Install various tools"
        }

        always {
          echo "---- Always"
          //junit 'junit.xml'
        }

        unstable {
          script {
            verifyStageUnstable = true
          }
        }
      }

    } // End install tools stage


    stage('Build the application artifact') {
        when {
          expression {
            doBuildTheApp
          }
        }

        steps {
           buildTheApp()
        }
    }

  } // End all Stages


// Always at end

  post {
    failure {
       echo "--- Fail"
    }
    success {
       echo "--- Success"
    }
  }

} // End pipeline
