resource "aws_ses_template" "tip_mon_results" {
  name    = "tip_mon_results"
  subject = "Greetings, TIP customer {{name}}!"
  html    = "<h1>Hello {{name}},</h1><p>This is your found results notification.</p>"
  text    = "Hello {{name}},\r\n this is the template for the TIP - Found results email"
}
