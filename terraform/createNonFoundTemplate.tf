resource "aws_ses_template" "tip_mon_no_results" {
  name    = "tip_mon_no_results"
  subject = "Greetings, {{name}}!"
  html    = "<h1>Hello {{name}},</h1><p>This is your no results notification.</p>"
  text    = "Hello {{name}},\r\n this is the template for the TIP - No Found results email"
}
