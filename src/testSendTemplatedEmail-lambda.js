"use strict";

// This simulates what the call to sendemail API would do (invokes the lambda code locally)
// node testEmail-lambda

var templated = require('./sendTemplatedEmail-lambda.js');

var m="testSendTemplatedEmail-lambda";

const event1={
   "templateName": "remaining",
   "subscrpn" : {
      "id": 1,
      "typeCd": "MO",
      "crmAcctId": "12345",
      "crmAcctNm": "The FBI Florida",
      "effDt": "2016-01-01",
      "expDt": "2020-01-01",
      "querySearchMaxNbr": 200,
      "querySearchUsedNbr": 23,
      "updBy": "sbotham",
      "updTs": "12345"
   },
   "srchrqst": {
      "rqstrEmail": "sbotham1968@gmail.com"
   },
   "debug": {
      "source": m
   }
}

const event2={
   "templateName": "basic",
   "subscrpn" : {
      "crmAcctNm": "The FBI Florida"
   },
   "srchrqst": {
      "rqstrEmail": "sbotham1968@gmail.com"
   },
   "debug": {
      "source": m
   }
}

var response = templated.handler(event1);

//console.log(response);



