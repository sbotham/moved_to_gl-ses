"use strict";

// This simulates what the call to sendemail API would do (invokes the lambda code locally)
// node testEmail-lambda

var sendBasicEmail = require('./sendBasicEmail-lambda.js');

var m="testSendBasicEmail-lambda";

const event={
   "templateName": "remaining",
   "subscrpn" : {
      "id": 1,
      "typeCd": "MO",
      "crmAcctId": "12345",
      "crmAcctNm": "The FBI Florida",
      "effDt": "2016-01-01",
      "expDt": "2020-01-01",
      "querySearchMaxNbr": 200,
      "querySearchUsedNbr": 23,
      "updBy": "sbotham",
      "updTs": "12345"
   },
   "srchrqst": {
      "rqstrEmail": "sbotham1968@gmail.com"
   },
   "debug": {
      "source": "testEmail-lambda.js 12345"
   }
}

const event2={
   "templateName": "basic",
   "subscrpn" : {
      "crmAcctNm": "The FBI Florida"
   },
   "srchrqst": {
      "rqstrEmail": "sbotham1968@gmail.com"
   },
   "debug": {
      "source": "testSendBasicEmail-lambda.js"
   }
}


//console.log("event="+JSON.stringify(event));

// Calls the lambda sendEmail-lambda.handler
//



//var response = email.handler(event2);
//
var response = sendBasicEmail.handler(event2);

//console.log(response);



