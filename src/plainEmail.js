var AWS = require('aws-sdk');

// Set the region 
AWS.config.update({region: 'us-east-1'});

var toEmailAddress=process.env.TIP_EMAIL_TO;
var fromEmailAddress=process.env.TIP_EMAIL_FROM;

/* The following example sends a formatted email: */

 var params = {
  Destination: {
   BccAddresses: [
   ], 
   CcAddresses: [
      toEmailAddress
   ], 
   ToAddresses: [
      toEmailAddress 
   ]
  }, 
  Message: {
   Body: {
    Html: {
     Charset: "UTF-8", 
     Data: "This message body contains HTML formatting. It can, for example, contain links like this one: <a class=\"ulink\" href=\"http://docs.aws.amazon.com/ses/latest/DeveloperGuide\" target=\"_blank\">Amazon SES Developer Guide</a>."
    }, 
    Text: {
     Charset: "UTF-8", 
     Data: "This is the message body in text format."
    }
   }, 
   Subject: {
    Charset: "UTF-8", 
    Data: "Test email"
   }
  },
  Source: fromEmailAddress,
  ReplyToAddresses: [
  ], 
 };

// Create the promise and SES service object
var sendPromise = new AWS.SES({apiVersion: '2010-12-01'}).sendEmail(params).promise();

// Handle promise's fulfilled/rejected states
sendPromise.then(
  function(data) {
    console.log("Sent id="+data.MessageId);
  }).catch(
    function(err) {
    console.error(err, err.stack);
  });
