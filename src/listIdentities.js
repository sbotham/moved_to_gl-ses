var AWS = require('aws-sdk');

// Set the region 
AWS.config.update({region: 'us-east-1'});

var toEmailAddress=process.env.TIP_EMAIL_TO;
var fromEmailAddress=process.env.TIP_EMAIL_FROM;

 var params = {
  IdentityType: "EmailAddress", 
  MaxItems: 123, 
  NextToken: ""
 };

// Create the promise and SES service object
var sendPromise = new AWS.SES({apiVersion: '2010-12-01'}).listIdentities(params).promise();

// Handle promise's fulfilled/rejected states
sendPromise.then(
  function(data) {
    console.log(data);
  }).catch(
    function(err) {
    console.error(err, err.stack);
  });
