"use strict";

const debug=true;
const consoleLog=true;

var utils = require('@tipsters/common/utils');
var subscrpn = require('@tipsters/domain/Subscrpn');
var srchrqst = require('@tipsters/domain/Srchrqst');
var general = require('@tipsters/domain/General');
  
/*
var utils = require('../../common/utils');
var subscrpn = require('../../domain/Subscrpn');
var srchrqst = require('../../domain/Srchrqst');
var general = require('../../domain/General');
*/

const m = "sendTemplatedEmail-lambda.handler";

// Starts here
  
exports.handler = async (event) => {

   let debugSource="";

   if (debug) {
      if (typeof event.debug === 'undefined' || !event.debug) {

      } else {
         console.log("source of this request="+event.debug.source);
         debugSource=event.debug.source;
      }
   }

   let sub = setSubData(event);
   let srch = setReqData(event);
   let gen = setGenData();

// 1. validate the request info (event).
   validateRequest(event);

// 2. create the template data.

   var templateData = utils.buildTemplateString(event.templateName, sub, gen, srch, debugSource);

	/*
   console.log(m+" 111111111111111111111111111");
   console.log(m+" "+templateData);
   console.log(m+"");
   */

/*
   if (event.templateName === "remaining") {
	   templateData = '{ "debug": "HC in sendEmail-lambda.js", "used_perc": "11.5", "remaining_perc": "88.5", "account_id": "12345", "total_count": "200", "agree_start": "2016-01-01", "agree_end": "2020-01-01", "account_name": "The FBI Florida", "search_status": "** ALERT **", "tip_team_contact_number": "703-816-5117", "tip_team_contact_email": "tipsearch@arccorp.com", "tip_contact_email": "jrice@arccorp.com", "tip_contact_number": "804-625-6044", "tip_contact_name": "Jason Rice", "arc_address": "3000 Wilson Blvd.,Suite 300<br>Arlington, VA 22201<br> arccorp.com<br>", "tip_team_contact_fax": "703.816.8138", "arc_moto": "THE<BR>INTELLIGENCE<BR>BEHIND<BR>AIR<BR>TRAVEL<BR>", "used_count": "23", "remaining_count": "177" }'
   }
   */

	/*
   console.log(m+" 222222222222222222222222222");
   console.log(m+" "+templateData);
   console.log(m+"");
   */
	  
// 3. call the email common code.

   console.log(m+" srch.rqstrEmail="+srch.rqstrEmail);

   var svcResponse = 
      utils.sendTemplatedEmail(event.templateName, templateData, srch.rqstrEmail, gen.tipTeamFromEmail);

   console.log(m+" svcResponse="+svcResponse);

   return "Email sent!";

};











function validateRequest(event) {
   console.log("event="+event);
   console.log("event.templateName="+event.templateName);
}



// TODO: Pull from table or config file?
  
function setGenData() {

   let gen1 = new general.General();
// gen1.tipTeamFromEmail="sbotham1968@gmail.com";
   gen1.tipTeamFromEmail="tipsearch@arccorp.com";
   gen1.tipTeamContactEmail="tipsearch@arccorp.com";
   gen1.tipTeamContactNumber="703-816-5117";
   gen1.tipTeamContactFax="703.816.8138";
   gen1.tipContactName="Jason Rice";
   gen1.tipContactEmail="jrice@arccorp.com";
   gen1.tipContactNumber="804-625-6044";
   gen1.arcAddress="3000 Wilson Blvd.,Suite 300<br>Arlington, VA 22201<br> arccorp.com<br>";
   gen1.arcMoto="THE<BR>INTELLIGENCE<BR>BEHIND<BR>AIR<BR>TRAVEL<BR>";
   return gen1;
}

function setSubData(event) {
   let sub1 = new subscrpn.Subscrpn(event.subscrpn.id,
	                            event.subscrpn.typeCd,
	                            event.subscrpn.crmAcctId,
	                            event.subscrpn.crmAcctNm,
	                            event.subscrpn.effDt, 
	                            event.subscrpn.expDt, 
	                            event.subscrpn.querySearchMaxNbr, 
	                            event.subscrpn.querySearchUsedNbr,
	                            event.updBy, 
	                            event.updTs);
   return sub1;
}

function setReqData(event) {
   let srch1 = new srchrqst.Srchrqst(1, event.srchrqst.rqstrEmail);
   console.log(srch1);
   return srch1;
}




function main() {

// Show incomming event
                
    console.log("event="+JSON.stringify(event)+"*");
   
// If comming from S3 event
    if (typeof event.Records==='undefined' || !event.Records) {
        console.log("Not triggered by S3");
    } else {
        console.log("event="+JSON.stringify(event.Records[0].eventSource)+"*");
    }

// If comming from api gateway - POST
    console.log("templateName="+event.templateName+"*");

// Check for a templateName
    if (typeof event.templateName!='undefined') {
        console.log(m+" you want to send an email with a template of '"+event.templateName+"'");
    }
   
// Pull out the subscription info
    if (typeof event.subscription!='undefined') {
        //&& event.templateName) {
        console.log(" sub="+JSON.stringify(event.subscription));
    }
}


function doStuff() {
   console.log(m + " do other stuff");
};

