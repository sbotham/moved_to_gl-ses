
var AWS = require('aws-sdk');                              

// Tip specific
var utils = require('@tipsters/common-utils/utils');
var subscrpn = require('@tipsters/domain/Subscrpn');
var srchrqst = require('@tipsters/domain/Srchrqst'); 



//var subscrpn = require('@tipsters/common-utils/domain/Subscrpn');
//
//
  
//const subscrpn = require('../../domain/Subscrpn'); // Move this out into common/global domain objects

// Start here

AWS.config.update({region: 'us-east-1'});

getEnvVars();

getCommandLineVars();

//utils.sendTheEmail();

doParamValidations();

setGenericTestData();

var testSub = setSubTestData();
var testReq = setReqTestData();

var templateData=buildTemplateString(testSub);
//console.log("templateData="+templateData);

sendTemplatedEmail();


function showUsage() {
   console.log("");
   console.log("usage: node templatedEmail {templateName} {sendEmail}");
   console.log("       node templatedEmail remaining false");
   console.log("");
}




function doParamValidations() {

   if (typeof fromEmailAddress !== "undefined" && fromEmailAddress) {
   } else {
      console.log("");
      console.log("Provide TIP_EMAIL_FROM");   
      showUsage();
      process.exit(1);
   }

   if (typeof toEmailAddress !== "undefined" && toEmailAddress) {
   } else {
      console.log("");
      console.log("Provide TIP_EMAIL_TO");   
      showUsage();
      process.exit(1);
   }

   if (typeof sendTheEmail !== "undefined" && sendTheEmail) {
      if (sendTheEmail=="true") { 
         sendTheEmail=true;
      } else { sendTheEmail=false; }
   } else {
      sendTheEmail=false;
   }

   if (typeof templateName !== "undefined" && templateName) {
   } else {
      console.log("");
      console.log("Please supply a template name");
      showUsage();
      process.exit(1);
   }
}

function getEnvVars() {

// This would be the customer contact email
	
   ccEmailAddress=process.env.TIP_EMAIL_TO;
   toEmailAddress=process.env.TIP_EMAIL_TO;

// This would be our generic reply to/from

   fromEmailAddress=process.env.TIP_EMAIL_FROM;
}

function getCommandLineVars() {
// console.log("Number of cmd line args="+process.argv.length);

   templateName=process.argv[2];
   sendTheEmail=process.argv[3];

   //console.log("templateName="+templateName);
   //console.log("sendTheEmail="+sendTheEmail);
}



function setReqTestData() {

   let Srchrqst = srchrqst.Srchrqst;
   let req1 = new Srchrqst(1, "sbotham1968@gmail.com");

}


function setSubTestData() {

   let Subscrpn = subscrpn.Subscrpn;
// let sub1 = new Subscrpn(1, "A", "12345", "The FBI Florida", "2016-01-01", "2020-01-01", 100, 20, "sbotham", Date.now());
   let sub1 = new Subscrpn(2, "MO", "12345", "The FBI Florida (Special)", "2018-01-01", "2025-01-01", 786, 342, "sbotham", Date.now());

   console.log(sub1.getPercUsed());
   console.log(sub1.getPercRemaining());

   searchStatus="** ALERT **";
// results
   link="https://google.com";
// no results
	
// ** ADDNEWTEMPLATE ** 1

   return sub1;

}

function setGenericTestData() {

// HC for now, this should come from either a table or config file, this will make it very fexible if
// we need to change contact etc.

// More generic for every email
//
   tipContactName="Jason Rice"
   tipContactNumber="804-625-6044"
   tipContactEmail="jrice@arccorp.com"
   tipTeamContactNumber="703-816-5117"
   tipTeamContactEmail="tipsearch@arccorp.com"
   tipTeamContactFax="703.816.8138"
   arcAddress="3000 Wilson Blvd.,Suite 300<br>Arlington, VA 22201<br> arccorp.com<br>"
   arcMoto="THE<BR>INTELLIGENCE<BR>BEHIND<BR>AIR<BR>TRAVEL<BR>"
}


// Uses values from Subscription row
  
function buildTemplateString(subscrpn) {

   var templateData;

   console.log(subscrpn.toString());

   if (templateName==='remaining') {
      templateData = '{'
      templateData = templateData + ' \"used_perc\": \"'+subscrpn.getPercUsed()+'\",'
      templateData = templateData + ' \"remaining_perc\": \"'+subscrpn.getPercRemaining()+'\",'
      templateData = templateData + ' \"account_id\": \"'+subscrpn.getCrmAcctId()+'\",'
      templateData = templateData + ' \"total_count\": \"'+subscrpn.getQuerySearchMaxNbr()+'\",'
      templateData = templateData + ' \"agree_start\": \"'+subscrpn.getEffDt()+'\",'
      templateData = templateData + ' \"agree_end\": \"'+subscrpn.getExpDt()+'\",'
      templateData = templateData + ' \"account_name\": \"'+subscrpn.getCrmAcctNm()+'\",'
      templateData = templateData + ' \"search_status\": \"'+searchStatus+'\",'
      templateData = templateData + ' \"tip_team_contact_number\": \"'+tipTeamContactNumber+'\",'
      templateData = templateData + ' \"tip_team_contact_email\": \"'+tipTeamContactEmail+'\",'
      templateData = templateData + ' \"tip_contact_email\": \"'+tipContactEmail+'\",'
      templateData = templateData + ' \"tip_contact_number\": \"'+tipContactNumber+'\",'
      templateData = templateData + ' \"tip_contact_name\": \"'+tipContactName+'\",'
      templateData = templateData + ' \"arc_address": \"'+arcAddress+'\",'
      templateData = templateData + ' \"tip_team_contact_fax\": \"'+tipTeamContactFax+'\",'
      templateData = templateData + ' \"arc_moto\": \"'+arcMoto+'\",'
      templateData = templateData + ' \"used_count\": \"'+subscrpn.getQuerySearchUsedNbr()+'\",'
      templateData = templateData + ' \"remaining_count\": \"'+subscrpn.getCountRemaining()+'\" '
      templateData = templateData + '}'
      return templateData;
   }

   if (templateName==='results') {
      templateData = '{'
      templateData = templateData + ' \"link\": \"'+link+'\"'
      templateData = templateData + ' }'
      return templateData;
   }

   if (templateName==='noresults') {
      templateData = '{'
      templateData = templateData + ' }'
      return templateData;
   }

// ** ADDNEWTEMPLATE ** 2

   if (templateName==='basic2') {
      templateData= '{ \"accountName\": \"'+accountName+'\", \"accountId\": \"'+accountId+'\" }'
   }

   if (templateName==='basic1') {
      templateData= "{ \"accountName\": \""+accountName+"\" } ";
   }

   return templateData;
}





function sendTemplatedEmail() {

 var params = {
  Destination: {
   BccAddresses: [
   ], 
   CcAddresses: [
   ], 
   ToAddresses: [
      toEmailAddress
   ]
  },
  ReturnPath: fromEmailAddress,
  Source: fromEmailAddress,
  Template: templateName,
  TemplateData: templateData,
  ReplyToAddresses: [
  ]
 };

console.log(params);

// Create the promise and SES service object
//
  if (sendTheEmail) {
     var sendPromise = new AWS.SES({apiVersion: '2010-12-01'}).sendTemplatedEmail(params).promise();  

// Handle promise's fulfilled/rejected states
//
     sendPromise.then(
        function(data) {
           console.log("*******************************************");
           console.log("Sent..id="+data.MessageId);
           console.log("Check email @ "+toEmailAddress);
	   console.log("*******************************************");
        }).catch(
           function(err) {
           console.error(err, err.stack);
        });

  } else {
	console.log("*******************************************");
	console.log("Not sending the email set sendTheEmail=true");
	console.log("*******************************************");
  }

  //console.log("End");
}



// Call would be to emailService.sendRemaining("12345");
//



// TODOS:-
//
// Other 2 templates
// Load template data from a file
//    for this you would have to escape and format the HTML
// Terraform or cli to create templates? (See README), these would be one off creates and
//    then are easy to update
// Logging
// Error Handling
// Use common code to do most of this.
// Expose this as a service.?
//
